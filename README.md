# Product Price History Analysis

This project focuses on analysis of historical prices of products available on e-commerce market. (At the moment it is smartwatch category from 4 shops).

Basic feature of it is to find best offer.<br>
But thanks to historical data we can observe when over time prices are more likely to be lower (seasonal sales, holidays, etc.). <br>
E-commerce owners can adjust prices to other competitors.

## Architecture / Pipeline

![pipeline](/pipeline.jpg)

Architecture of this project is build on Linux server with docerized: PostgreSQL database, Apache Airflow and Apache Spark. These images were created from this project with small modifications.

Data are collected from e-commerce websites using Python Scrapy library.<br>
Scraped informations are stored on Linux file system as csv files.

ETL process:

Before extracting data , csv files are replaced by parquet format. <br>
One reason of doing that is to reduce size od stored data and second if someone would like to use it without data warehouse could have faster access especially when interested in just some informations.

From this file data are extracted into Stage area (PostgreSQL) and than after transformations stored in Data warehouse (also build in PostgreSQL) . They are organized in star schema.<br>
Format switching and extraction are handled by Spark (PySpark scripts). <br>
Whole ETL process is orchestrated in Apache Airflow. <br>
Except storing transformed data, some aggregations and other useful information are generated and keep in tables or views to be used for analysis.

Front-end is based on AWS Quick Sight and Tableau .


