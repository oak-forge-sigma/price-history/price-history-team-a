import scrapy
import configparser
from scrapy.crawler import CrawlerProcess
from datetime import datetime
import pathlib

config = configparser.ConfigParser()
config.read('config.ini')

date = datetime.now().strftime('%Y_%m_%d_%H')
dir_name = datetime.now().strftime('%Y_%m_%d')
dir_ym = datetime.now().strftime('%Y_%m')
path = pathlib.Path(f'data/{dir_ym}/{dir_name}')
path.mkdir(parents=True, exist_ok=True)

class VobisSpider(scrapy.Spider):
    name = config['vobis']['name']
    csv_file = f'{path}/{name}_{date}.csv'

    custom_settings = {
        'FEEDS': {csv_file: {'format': 'csv'}
                 }
        }

    def start_requests(self):
        url = config['vobis']['url']
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        products = response.css('div.m-offerBox_item.js-adBlock_offerBox_item.js-analyticsProduct')
        for product in products:
            # check if price available
            if len(product.css('p.m-offerBox_price')) > 0:
                price = product.css('p.m-offerBox_price::text').extract_first().replace(' zł', '').replace(',', '.').replace(' ', '')
            else:
                price = 'nan'
            # check if original price available
            if len(product.css('p.m-offerBox_oldPrice')) > 0:
                price_origin = product.css('p.m-offerBox_oldPrice::text').extract_first().replace(' zł', '').replace(',', '.').replace(' ', '')
            else:
                price_origin = price
            # check if rating available
            if len(product.css('div.m-offerBox_rating')) > 0:
                rating = product.css('div.m-offerBox_rating::text').extract_first().split()[0]
                num_rating = product.css('div.m-offerBox_ratingSum>a.js-analyticsLink::text').extract_first().split()[0].replace('(', '')
            else:
                rating = 'nan'
                num_rating = 0
            available = product.css('p.m-offerBox_delivery::text').extract_first()
            if available.startswith('wysyłka'):
                availibility = 1
            else:
                availibility = 0
            if (availibility == 0) and (price == 'nan'):
                continue
            item = {
                config['item_name']['name']: product.css('a.js-analyticsLink.js-analyticsData::text').extract_first().strip().replace('Smartwatch ', ''),
                config['item_name']['price']: price,
                config['item_name']['price_origin']: price_origin,
                config['item_name']['rating']: rating,
                config['item_name']['number_ratings']: num_rating,
                config['item_name']['availability']: availibility,
                config['item_name']['date']: datetime.now(),
                config['item_name']['shop']: config['item_name'].get('vobis')
            }

            yield item

        next_page = response.css('a.m-pagination_next::attr(href)').extract_first()
        if len(next_page) > 0:
            next_page = response.urljoin(next_page)
            yield response.follow(next_page, callback = self.parse)

class MediaexpertSpider(scrapy.Spider):
    name = config['mediaexpert']['name']
    csv_file = f'{path}/{name}_{date}.csv'

    custom_settings = {
        'FEEDS': {csv_file: {'format': 'csv'}
                  }
    }

    def start_requests(self):
        url = config['mediaexpert']['url']
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        products = response.css('div.offer-box')
        links = products.css('h2.name.is-section>a.is-animate.spark-link::attr(href)').extract()
        for product_link in links:
            product_link = response.urljoin(product_link)
            yield response.follow(product_link, callback=self.parse_pages)

        next_page = response.css('a.spark-button.is-icon.is-simple.is-default.icon-left::attr(href)').extract()[-1]
        next_page_hidden = response.css('a.spark-button.is-icon.is-simple.is-default.icon-left.is-hidden::attr(href)').extract()
        next_page_hidden = next_page_hidden[-1] if len(next_page_hidden) >= 1 else next_page_hidden
        if (len(next_page_hidden) == 0) or (next_page != next_page_hidden):
            next_page = response.urljoin(next_page)
            yield response.follow(next_page, callback=self.parse)

    def parse_pages(self, response):
        if response.css('h1.name.is-title::text').extract_first() is None:
            return
        else:
            name = response.css('h1.name.is-title::text').extract_first().strip().replace('Smartwatch ', '')
        # check if price available
        if len(response.css('div.main-price.is-big')) > 0:
            price = response.css('div.main-price.is-big>span.whole::text').extract_first().replace('\u202f',
                                                                                                   '') + '.' + response.css(
                'div.rest>span.cents::text').extract_first()
        else:
            price = 'nan'
        # check if original price available
        if len(response.css('div.old-price.is-medium')) > 0:
            price_origin = response.css(
                'div.old-price.is-medium>span.whole::text').extract_first().replace('\u202f', '') + '.' + response.css(
                'div.old-price.is-medium>span.cents::text').extract_first()
        else:
            price_origin = price
        if len(response.css('div.container>div.title::text')) > 0:
            rating = response.css('div.container>div.title::text').extract_first().replace('/5', '')
            number_ratings = response.css('div.container>div.count::text').extract_first().split()[0]
        else:
            rating = 'nan'
            number_ratings = 0
        if response.css('span.unavailable::text').extract_first() is None:
            available = 1
        else:
            available = 0

        item = {
            config['item_name']['name']: response.css('h1.name.is-title::text').extract_first().strip().replace('Smartwatch ', ''),
            config['item_name']['price']: price,
            config['item_name']['price_origin']: price_origin,
            config['item_name']['rating']: rating,
            config['item_name']['number_ratings']: number_ratings,
            config['item_name']['availability']: available,
            config['item_name']['date']: datetime.now(),
            config['item_name']['shop']: config['item_name']['media'] 
        }

        yield item

class EuroSpider(scrapy.Spider):
    name = config['euro']['name']
    csv_file = f'{path}/{name}_{date}.csv'

    custom_settings = {
        'FEEDS': {csv_file: {'format': 'csv'}
                  }
    }

    def start_requests(self):
        url = config['euro']['url']
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        products = response.css('div.product-for-list')

        for product in products:
            # name of smartwatch
            if len(product.css('a.js-save-keyword')) > 0:
                name = product.css('h2.product-name>a.js-save-keyword::text').extract_first().strip()
            else:
                continue
            # price rn
            if len(product.css('div.price-normal.selenium-price-normal')) > 0:
                price = product.css('div.price-normal.selenium-price-normal::text').extract_first()[:-3].replace(' zł', '').strip().replace(u'\xa0', u'').replace(',', '.')
                # price = eval(price)
                available = 1
            else:
                price = 'nan'
                available = 0
            # price before discount?
            if len(product.css('div.price-old')) > 0:
                price_origin = product.css('div.price-old::text').extract_first()[:-3].replace(' zł', '').strip().replace(u'\xa0', u'').replace(',', '.')
                # price_origin = eval(price_origin)
            else:
                price_origin = price
            # check if rating available
            header = product.css('div.product-header')
            try:
                rating = header.css("a.js-save-keyword.js-scroll-by-hash::attr('title')").extract_first().split(" ")[0]
                if rating == 'Brak':
                    rating = 'nan'
                    num_rating = 0
                else:
                    num_rating = header.css("a.js-save-keyword.js-scroll-by-hash::attr('title')").extract_first().split(" ")[4]
            except AttributeError:
                rating = 'nan'
                num_rating = 0

            item = {
                config['item_name']['name']: name,
                config['item_name']['price']: str(price),
                config['item_name']['price_origin']: str(price_origin),
                config['item_name']['rating']: rating,
                config['item_name']['number_ratings']: num_rating,
                config['item_name']['availability']: available,
                config['item_name']['date']: datetime.now(),
                config['item_name']['shop']: config['item_name']['euro']
            }
            yield item

        next_page = response.css('a.paging-next.selenium-WC-paging-next-button::attr(href)').extract_first()
        if next_page:
            next_page = response.urljoin(next_page)
            yield response.follow(next_page, callback=self.parse)

class MoreleSpider(scrapy.Spider):
    name = config['morele']['name']
    csv_file = f'{path}/{name}_{date}.csv'

    custom_settings = {
        'FEEDS': {csv_file: {'format': 'csv'}
                  }
    }

    def start_requests(self):
        url = config['morele']['url']
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        products = response.css('div.cat-product-inside')
        links = products.css('a.productLink::attr(href)').extract()
        for product_link in links:
            yield response.follow(product_link, callback=self.parse_pages)

        link = response.css('li.pagination-lg.next>link::attr(href)').extract()
        if len(link) > 0:
            yield response.follow(response.urljoin(link[0]), callback=self.parse)

    def parse_pages(self, response):
        if len(response.css('div.product-price-old')) > 0:
            price_origin = response.css('div.product-price-old::text').extract_first().strip().replace(' zł', '').replace(',', '.')
        else:
            price_origin = response.css('div.product-price::attr(content)').extract_first().strip()
        if len(response.css('div.review-rating-number')) > 0:
            rating = response.css('div.review-rating-number::text').extract_first().strip().replace('/5', '')
        else:
            rating = 'nan'
        if len(response.css('div.prod-available-items::text')) > 0:
            availability = 1
        else:
            availability = 0
        if len(response.css('span.ml-1.counter::text')) > 0:
            number_ratings = response.css('span.ml-1.counter::text').extract_first()[1:-1]
        else:
            number_ratings = 0
        item = {
            config['item_name']['name']: response.css('h1.prod-name::text').extract_first().strip().replace('"', '').replace('Smartwatch ',''),
            config['item_name']['price']: str(response.css('div.product-price::attr(content)').extract_first().strip()),
            config['item_name']['price_origin']: str(price_origin),
            config['item_name']['rating']: rating,
            config['item_name']['number_ratings']: number_ratings,
            config['item_name']['availability']: availability,
            config['item_name']['date']: datetime.now(),
            config['item_name']['shop']: config['item_name']['morele']
        }
        yield item

process = CrawlerProcess()
process.crawl(VobisSpider)
process.crawl(MediaexpertSpider)
process.crawl(EuroSpider)
process.crawl(MoreleSpider)

process.start()
