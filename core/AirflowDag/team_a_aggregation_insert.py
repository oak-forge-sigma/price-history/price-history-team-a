from airflow import DAG
from datetime import date, datetime
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.postgres_operator import PostgresOperator

default_args = {
    'owner': 'oak_price_history_team_a',
    'retries': 1,
}


dag = DAG('team_a_aggregation_insert',
          default_args = default_args,
          schedule_interval='15 22 * * *',
          start_date=datetime(2022, 2, 22),
          catchup=False)


monthly = PostgresOperator(
              task_id="monthly_insert_period",
              postgres_conn_id="postgres_id",
              sql="team_a_file_sql/agg_dwh_monthly_insert_period.sql",
              dag=dag)

quarterly = PostgresOperator(
              task_id="quarterly_insert_period",
              postgres_conn_id="postgres_id",
              sql="team_a_file_sql/agg_dwh_quarterly_insert_period.sql",
              dag=dag)

yearly = PostgresOperator(
              task_id="yearly_insert_period",
              postgres_conn_id="postgres_id",
              sql="team_a_file_sql/agg_dwh_yearly_insert_period.sql",
              dag=dag)

weekly = PostgresOperator(
		task_id="weekly_insert_period",
		postgres_conn_id="postgres_id",
		sql="team_a_file_sql/agg_dwh_weekly_insert_period.sql",
		dag=dag)

refresh_view = PostgresOperator(
		task_id="refresh_materialized_view",
		postgres_conn_id="postgres_id",
		sql="team_a_file_sql/refresh_materialized_view.sql",
		dag=dag)

monthly >> quarterly >> yearly >> weekly >> refresh_view
