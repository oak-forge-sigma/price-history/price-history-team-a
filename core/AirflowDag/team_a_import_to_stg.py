from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.hooks.base_hook import BaseHook
from datetime import date, datetime
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.postgres_operator import PostgresOperator
from airflow.models import Variable

connection = BaseHook.get_connection('postgres_stg')
###############################################
# Parameters
###############################################

postgres_driver_jar = Variable.get("postgres_driver")
dl_team_a = Variable.get("dl_team_a_path")
postgres_db = 'jdbc:postgresql://' + str(connection.host) + ':' + str(connection.port) + '/' + str(connection.schema)
table = 'price_history_stg.products_stage'
user = connection.login
password = connection.password

###############################################
# DAG Definition
###############################################

default_args = {
    'owner': 'oak_price_history_team_a',
    'retries': 1,
    'depends_on_past': False,
}


dag = DAG('team_a_import_to_stg',
          description = 'Load data to postgres data warehouse',
          default_args = default_args,
          schedule_interval='15 20 * * *',
          start_date=datetime(2022, 2, 1),
          catchup=False)

csv_to_parquet_spark_task = SparkSubmitOperator(
    task_id='csv_to_parquet_spark_task',
    conn_id='spark_default',
    application='/usr/local/spark/app/team_a_parquet_daily.py',
    name='csv_parquet',
    verbose=1,
    dag=dag)

load_to_stg = SparkSubmitOperator(
    task_id="load_to_stg",
    application="/usr/local/spark/app/read_load_data.py", # Spark application path created in airflow and spark cluster
    conn_id="spark_default",
    verbose=1,
    application_args=[dl_team_a, postgres_db, table, user, password],
    jars=postgres_driver_jar,
    driver_class_path=postgres_driver_jar,
    dag=dag)

move_from_stg_to_final = PostgresOperator(
              task_id="move_from_stg_to_final",
              postgres_conn_id="postgres_id",
              sql="from_stg_to_final_1_day.sql",
              dag=dag)

csv_to_parquet_spark_task >> load_to_stg >> move_from_stg_to_final
