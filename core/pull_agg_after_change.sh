#!/usr/bin/env bash

changed_files="$(git diff-tree -r --name-only --no-commit-id $1 $2)"

check_run() {
	echo "$changed_files" | grep --quiet "$1" && eval "$2"
}

check_run ../db/agg_dwh_monthly_insert_period.sql "cp ../db/agg_dwh_monthly_insert_period.sql ../../../kbury/docker_environment_setup/airflow-spark/dags/team_a_file_sql"
check_run ../db/agg_dwh_quarterly_insert_period.sql "cp ../db/agg_dwh_quarterly_insert_period.sql ../../../kbury/docker_environment_setup/airflow-spark/dags/team_a_file_sql"
check_run ../db/agg_dwh_yearly_insert_period.sql "cp ../db/agg_dwh_yearly_insert_period.sql ../../../kbury/docker_environment_setup/airflow-spark/dags/team_a_file_sql"

exit 0;

