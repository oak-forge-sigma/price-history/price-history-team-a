import os
from glob import glob
import shutil
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

resources_path = config['data_team_a']['resources_path']
path = f"{resources_path}"
ext = "*.csv"
all_csv_files = [file for path, subdir, files in os.walk(path)
                 for file in glob(os.path.join(path, ext))]

for filename in all_csv_files:
    try:
        if os.path.isfile(filename):
            os.remove(filename)
        else:
            shutil.rmtree(filename)
    except FileNotFoundError:
        pass
