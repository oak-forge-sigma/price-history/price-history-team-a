from pyspark.sql import SparkSession
import os
from glob import glob
from datetime import datetime
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

spark = SparkSession.builder.appName('team_a_csv_parquet_daily').getOrCreate()

dir_name = datetime.now().strftime('%Y_%m_%d')
dir_ym = datetime.now().strftime('%Y_%m')
resources_path = config['data_team_a']['resources_path']
path = f"{resources_path}/{dir_ym}/{dir_name}"
ext = "*.csv"
daily_csv_files = [file for path, subdir, files in os.walk(path)
                   for file in glob(os.path.join(path, ext))]

df = spark.read.option("delimiter", ",").option("header", True).csv(daily_csv_files)
df.write.mode('overwrite').partitionBy('shop').parquet(f'{resources_path}/{dir_ym}/{dir_name}/smartwatch_{dir_name}.parquet')
for filename in daily_csv_files:
    try:
        os.remove(filename)
    except OSError:
        pass
