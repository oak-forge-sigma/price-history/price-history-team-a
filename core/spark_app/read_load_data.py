import sys
from pyspark.sql import SparkSession
from datetime import date, timedelta
from pyspark.sql.types import TimestampType
from  pyspark.sql.functions import input_file_name
from pyspark.sql import functions as F

spark = SparkSession.builder.getOrCreate()

dl_path = sys.argv[1]
postgres_db = sys.argv[2]
table = sys.argv[3]
user = sys.argv[4]
password = sys.argv[5]

ym = date.today().strftime('%Y_%m')
ymd = date.today().strftime('%Y_%m_%d')

path = f'{dl_path}/{ym}/{ymd}/*.parquet'


df = spark.read.parquet(path).withColumn("source_file", input_file_name())

df = df.withColumn("source_file", F.split("source_file","/"))
df = df.withColumn("source_file", F.col("source_file").getItem(10))
new_col_names = ['name','sale_price','origin_price', 'rating', 'reviews_count', 'available', 'date', 'shop', 'source_file']
for old,new in zip(df.columns,new_col_names):
    df = df.withColumnRenamed(old,new)
df = df.withColumn("date",df.date.cast(TimestampType()))


df_files = spark.read.format('jdbc')\
    .option('url', postgres_db)\
    .option('query', 'select distinct(source_file) from price_history_stg.products_stage') \
    .option('user', user)\
    .option('password', password)\
    .load()

df = df.join(df_files, df.source_file == df_files.source_file, "leftanti")

df.write.format('jdbc')\
    .option('url', postgres_db)\
    .option('dbtable', table) \
    .option('user', user)\
    .option('password', password)\
    .mode('append')\
    .save()
