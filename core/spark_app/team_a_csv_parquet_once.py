from pyspark.sql import SparkSession
import os
from glob import glob
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

spark = SparkSession.builder.appName('team_a_csv_parquet_once').getOrCreate()

resources_path = config['data_team_a']['resources_path']
path = f"{resources_path}"
ext = "*.csv"
one_day_path = []
one_day_csv = []

for (dirpath, subdir, files) in os.walk(path):
    for file in glob(os.path.join(dirpath, ext)):
        base, tail = os.path.split(file)
        if base not in one_day_path and bool(one_day_path):
            csv_files = list(map('/'.join, zip(one_day_path, one_day_csv)))
            file_path = one_day_path[0]
            file_date = file_path.split('/')[-1]
            try:
                df = spark.read.option("delimiter", ",").option("header", True).csv(csv_files)
                df.write.mode('overwrite').partitionBy('shop').parquet(f'{file_path}/smartwatch_{file_date}.parquet')
            except Exception:
                df = spark.read.option("delimiter", ";").option("header", True).csv(csv_files)
                df.write.mode('overwrite').partitionBy('shop').parquet(f'{file_path}/smartwatch_{file_date}.parquet')
            one_day_csv.clear()
            one_day_path.clear()
        one_day_csv.append(tail)
        one_day_path.append(base)
