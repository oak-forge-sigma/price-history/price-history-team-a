import psycopg2
import os
from glob import glob

conn = psycopg2.connect('host= dbname= user= password=')
cur = conn.cursor()

cur.execute('SELECT distinct source_file FROM price_history_stg.products_stage')
files_stg = [row[0] for row in cur.fetchall()]

path = 'data'
ext = '*.csv'
all_csv_files = [file for path, subdir, files in os.walk(path)
                 for file in glob(os.path.join(path, ext))]

for filename in all_csv_files:
    f = os.path.basename(filename)
    if f not in files_stg:
        print(filename)
        with open(filename, 'r', encoding='utf-8') as file:
            next(file) # Skip the header row

            cur.execute('ALTER TABLE price_history_stg.products_stage ALTER source_file SET DEFAULT %s', (f,))
            cur.copy_expert('COPY price_history_stg.products_stage (name, sale_price, origin_price, rating, reviews_count, available, date, shop) FROM stdin with csv ', file)

        conn.commit()

cur.execute('ALTER TABLE price_history_stg.products_stage ALTER source_file DROP DEFAULT')
conn.commit()
cur.close()
conn.close()

