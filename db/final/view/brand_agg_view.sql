--brand aggregates
CREATE OR REPLACE VIEW price_history_dwh.brand_aggregate as
WITH prod_date_appear as(
	SELECT fp.product_id pid, 
		min(c.date_date) first_date, 
		max(c.date_date) last_date 
	FROM price_history_dwh.f_prices fp  
	INNER JOIN price_history_dwh.calendar c 
		ON fp.time_id = c.time_id 
	GROUP BY fp.product_id 
),
new_brand_prod AS (
	SELECT p.brand , count(DISTINCT p.product_id) new_prod_month
	FROM price_history_dwh.product p 
	LEFT JOIN prod_date_appear pda
		ON p.product_id = pda.pid
	WHERE pda.first_date > current_date - INTERVAL '30 day'
	GROUP BY p.brand 
),
decom_brand_prod AS (
	SELECT p.brand , count(DISTINCT p.product_id) dec_prod_month
	FROM price_history_dwh.product p 
	LEFT JOIN prod_date_appear pda
		ON p.product_id = pda.pid
	WHERE pda.last_date < current_date - INTERVAL '1 day'
	GROUP BY p.brand 
),
brand_agg as(
	SELECT p.brand , 
		count(DISTINCT p.product_id) num_prod, 
		avg(fp.sale_price) avg_price,
		percentile_cont(0.5) WITHIN group(ORDER BY fp.sale_price) median_price_cont,
		percentile_disc(0.5) WITHIN group(ORDER BY fp.sale_price) median_price_disc
	FROM price_history_dwh.product p 
	INNER JOIN price_history_dwh.f_prices fp 
		ON p.product_id = fp.product_id 
	GROUP BY p.brand 
)
SELECT ba.brand brand, 
	nbp.new_prod_month num_new_product,
	dbp.dec_prod_month num_decomission_product,
	ba.num_prod ,
	round(ba.avg_price,2) avg_price ,
	ba.median_price_cont ,
	ba.median_price_disc 
FROM brand_agg ba
LEFT JOIN new_brand_prod nbp
	ON ba.brand = nbp.brand
LEFT JOIN decom_brand_prod dbp 
	ON ba.brand = dbp.brand
;