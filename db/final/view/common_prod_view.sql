--common/unique products
CREATE OR REPLACE VIEW price_history_dwh.common_prod AS
SELECT
    price_history_dwh.f_prices.product_id,
    count(DISTINCT price_history_dwh.f_prices.shop_id) AS count_shop
FROM price_history_dwh.f_prices
INNER JOIN price_history_dwh.calendar
    ON price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id
WHERE
    price_history_dwh.calendar.date_date BETWEEN date_trunc(
        'month', current_date - INTERVAL '12 month'
    )::DATE AND (date_trunc('month', current_date ) - INTERVAL '1 day')::DATE
GROUP BY price_history_dwh.f_prices.product_id;