CREATE OR REPLACE VIEW price_history_dwh.agg_view AS
WITH daily_price AS (
    SELECT
        price_history_dwh.f_prices.product_id,
        price_history_dwh.product.brand,
        price_history_dwh.shop.shop_name,
        price_history_dwh.calendar.dayname_of_week,
        price_history_dwh.calendar.year_week,
        price_history_dwh.calendar.date_date,
        avg(price_history_dwh.f_prices.sale_price) AS avg_daily
    FROM price_history_dwh.f_prices
    LEFT JOIN price_history_dwh.calendar
        ON
            price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id
    LEFT JOIN price_history_dwh.product
        ON
            price_history_dwh.f_prices.product_id = price_history_dwh.product.product_id
    LEFT JOIN price_history_dwh.shop
        ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id
    GROUP BY
        price_history_dwh.f_prices.product_id,
        price_history_dwh.product.brand,
        price_history_dwh.shop.shop_name,
        price_history_dwh.calendar.dayname_of_week,
        price_history_dwh.calendar.year_week,
        price_history_dwh.calendar.date_date
)

SELECT
    daily_price.product_id,
    price_history_dwh.weekly_agg.product_name,
    daily_price.brand,
    daily_price.shop_name,
    daily_price.dayname_of_week,
    daily_price.date_date,
    daily_price.year_week,
    daily_price.avg_daily,
    price_history_dwh.weekly_agg.avg_sale_price AS avg_weekly,
    price_history_dwh.common_prod.count_shop,
    price_history_dwh.last_year_agg.min_year_price,
    price_history_dwh.last_year_agg.max_year_price,
    price_history_dwh.last_year_agg.avg_year_price,
    price_history_dwh.last_year_agg.first_date_year,
    price_history_dwh.last_year_agg.last_date_year,
    price_history_dwh.last_year_agg.first_price_year,
    price_history_dwh.last_year_agg.current_price,
    price_history_dwh.last_year_agg.min_year_price_date,
    price_history_dwh.last_year_agg.max_year_price_date,
    price_history_dwh.last_year_agg.perc_price_diff_year,
    price_history_dwh.last_month_agg.min_month_price,
    price_history_dwh.last_month_agg.max_month_price,
    price_history_dwh.last_month_agg.avg_month_price,
    price_history_dwh.last_month_agg.first_date_month,
    price_history_dwh.last_month_agg.last_date_month,
    price_history_dwh.last_month_agg.first_price_month,
    price_history_dwh.last_month_agg.min_month_price_date,
    price_history_dwh.last_month_agg.max_month_price_date,
    price_history_dwh.last_month_agg.perc_price_diff_month,
    price_history_dwh.last_week_agg.min_week_price,
    price_history_dwh.last_week_agg.max_week_price,
    price_history_dwh.last_week_agg.avg_week_price,
    price_history_dwh.last_week_agg.first_date_week,
    price_history_dwh.last_week_agg.last_date_week,
    price_history_dwh.last_week_agg.first_price_week,
    price_history_dwh.last_week_agg.min_week_price_date,
    price_history_dwh.last_week_agg.max_week_price_date,
    price_history_dwh.last_week_agg.perc_price_diff_week,
    round(
        (
            daily_price.avg_daily / price_history_dwh.weekly_agg.avg_sale_price - 1
        ) * 100,
        2
    ) AS perc_diff,
    round(
        (
            price_history_dwh.last_year_agg.current_price / price_history_dwh.last_month_agg.avg_month_price - 1
        ) * 100,
        2
    ) AS perc_price_diff_cur_vs_avg30
FROM daily_price
LEFT JOIN price_history_dwh.weekly_agg
    ON
        daily_price.product_id = price_history_dwh.weekly_agg.product_id AND daily_price.shop_name = price_history_dwh.weekly_agg.shop_name AND daily_price.year_week = price_history_dwh.weekly_agg.year_week
LEFT JOIN price_history_dwh.common_prod
    ON daily_price.product_id = price_history_dwh.common_prod.product_id
LEFT JOIN price_history_dwh.last_year_agg
    ON
        daily_price.product_id = price_history_dwh.last_year_agg.product_id AND daily_price.shop_name = price_history_dwh.last_year_agg.shop_name
LEFT JOIN price_history_dwh.last_month_agg
    ON
        daily_price.product_id = price_history_dwh.last_month_agg.product_id AND daily_price.shop_name = price_history_dwh.last_month_agg.shop_name
LEFT JOIN price_history_dwh.last_week_agg
    ON daily_price.product_id = price_history_dwh.last_week_agg.product_id AND daily_price.shop_name = price_history_dwh.last_week_agg.shop_name;
