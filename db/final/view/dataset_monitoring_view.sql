--dataset monitoring last 9 days
CREATE OR REPLACE VIEW price_history_dwh.shops AS
SELECT
    price_history_dwh.shop.shop_name,
    price_history_dwh.calendar.date_date,
    count(DISTINCT price_history_dwh.f_prices.product_id) AS number_of_products,
    round(avg(price_history_dwh.f_prices.sale_price), 2) AS avg_price,
	sum(price_history_dwh.f_prices.sale_price) total_price
FROM price_history_dwh.calendar 
LEFT JOIN price_history_dwh.f_prices 
	ON price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id 
LEFT JOIN price_history_dwh.shop  
	ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id 
WHERE price_history_dwh.calendar.date_date BETWEEN current_date - INTERVAL '9 day' AND current_date - INTERVAL '1 day'
GROUP BY price_history_dwh.shop.shop_name, price_history_dwh.calendar.date_date;

