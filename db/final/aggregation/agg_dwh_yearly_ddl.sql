DROP TABLE IF EXISTS price_history_dwh.yearly_agg;
CREATE TABLE price_history_dwh.yearly_agg(

    product_name VARCHAR(200),
    shop_name VARCHAR(50),
    year_int CHAR(7),
    min_sale_price NUMERIC(8, 2),
    max_sale_price NUMERIC(8, 2),
    avg_sale_price NUMERIC(8, 2),
    std_sale_price NUMERIC(8, 2),
    discount_days SMALLINT,
    available_count_days SMALLINT,
    UNIQUE (product_name, shop_name, year_int)
);