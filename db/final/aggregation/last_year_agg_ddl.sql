DROP TABLE IF EXISTS price_history_dwh.last_year_agg;
CREATE TABLE price_history_dwh.last_year_agg(
    product_id INT,
    product_name VARCHAR(200),
    brand VARCHAR(50),
    shop_name VARCHAR(50),
    min_year_price NUMERIC(8, 2),
    max_year_price NUMERIC(8, 2),
    avg_year_price NUMERIC(8, 2),
    first_date_year DATE,
    last_date_year DATE,
    first_price_year NUMERIC(8, 2),
    current_price NUMERIC(8, 2),
    min_year_price_date DATE,
    max_year_price_date DATE,
    perc_price_diff_year NUMERIC(5, 2)
);
