DROP TABLE IF EXISTS price_history_dwh.last_month_agg;
CREATE TABLE price_history_dwh.last_month_agg(
    product_id INT,
    product_name VARCHAR(200),
    brand VARCHAR(50),
    shop_name VARCHAR(50),
    min_month_price NUMERIC(8, 2),
    max_month_price NUMERIC(8, 2),
    avg_month_price NUMERIC(8, 2),
    first_date_month DATE,
    last_date_month DATE,
    first_price_month NUMERIC(8, 2),
    current_price NUMERIC(8, 2),
    min_month_price_date DATE,
    max_month_price_date DATE,
    perc_price_diff_month NUMERIC(5, 2)
);
