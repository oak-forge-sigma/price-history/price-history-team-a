DROP TABLE IF EXISTS price_history_dwh.weekly_agg;
CREATE TABLE price_history_dwh.weekly_agg(
    product_id INT,
    product_name VARCHAR(200),
    shop_name VARCHAR(50),
    year_week CHAR(7),
    min_sale_price NUMERIC(8, 2),
    max_sale_price NUMERIC(8, 2),
    avg_sale_price NUMERIC(8, 2),
    UNIQUE (product_id, product_name, shop_name, year_week)
);
