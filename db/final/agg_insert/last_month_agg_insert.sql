TRUNCATE TABLE price_history_dwh.last_month_agg;

INSERT INTO price_history_dwh.last_month_agg
WITH month_agg AS (
    SELECT
        price_history_dwh.f_prices.product_id,
        price_history_dwh.product.product_full_name,
        price_history_dwh.product.brand,
        price_history_dwh.shop.shop_name,
        price_history_dwh.calendar.date_date,
        price_history_dwh.f_prices.sale_price,
        min(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS min_m_price,
        max(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS max_m_price,
        avg(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS avg_m_price,
        first_value(
            price_history_dwh.calendar.date_date
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY price_history_dwh.calendar.date_date
        ) AS first_date,
        last_value(
            price_history_dwh.calendar.date_date
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY
                price_history_dwh.calendar.date_date
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
        ) AS last_date,
        first_value(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY price_history_dwh.calendar.date_date
        ) AS first_price,
        last_value(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY
                price_history_dwh.calendar.date_date
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
        ) AS current_price
    FROM price_history_dwh.f_prices
    INNER JOIN price_history_dwh.calendar
        ON
            price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id
    INNER JOIN price_history_dwh.product
        ON
            price_history_dwh.f_prices.product_id = price_history_dwh.product.product_id
    INNER JOIN price_history_dwh.shop
        ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id
    WHERE
        price_history_dwh.calendar.date_date > current_date - INTERVAL '30 day'
),

month_agg_min_date AS (
    SELECT
        month_agg.product_id,
        month_agg.shop_name,
        min(month_agg.date_date) AS min_m_price_date
    FROM month_agg
    WHERE month_agg.sale_price = month_agg.min_m_price
    GROUP BY month_agg.product_id, month_agg.shop_name
),

month_agg_max_date AS (
    SELECT
        month_agg.product_id,
        month_agg.shop_name,
        min(month_agg.date_date) AS max_m_price_date
    FROM month_agg
    WHERE month_agg.sale_price = month_agg.max_m_price
    GROUP BY month_agg.product_id, month_agg.shop_name
)

SELECT
    month_agg.product_id,
    month_agg.product_full_name,
    month_agg.brand,
    month_agg.shop_name,
    month_agg.min_m_price,
    month_agg.max_m_price,
    month_agg.avg_m_price,
    month_agg.first_date,
    month_agg.last_date,
    month_agg.first_price,
    month_agg.current_price,
    month_agg_min_date.min_m_price_date,
    month_agg_max_date.max_m_price_date,
    round(
        (month_agg.current_price / month_agg.first_price - 1) * 100, 2
    ) AS perc_price_diff
FROM month_agg
INNER JOIN month_agg_min_date
    ON
        month_agg.product_id = month_agg_min_date.product_id AND month_agg.shop_name = month_agg_min_date.shop_name
INNER JOIN month_agg_max_date
    ON
        month_agg.product_id = month_agg_max_date.product_id AND month_agg.shop_name = month_agg_max_date.shop_name
GROUP BY
    month_agg.product_id,
    month_agg.product_full_name,
    month_agg.brand,
    month_agg.shop_name,
    month_agg.min_m_price,
    month_agg.max_m_price,
    month_agg.avg_m_price,
    month_agg.first_date,
    month_agg.last_date,
    month_agg.first_price,
    month_agg.current_price,
    month_agg_min_date.min_m_price_date,
    month_agg_max_date.max_m_price_date;
