WITH weekly as(
	SELECT p.product_id product_id,
		p.product_full_name product_name,
		s.shop_name shop_name,
		c.year_week year_week,
		min(f.sale_price) min_price,
		max(f.sale_price) max_price,
		avg(f.sale_price) avg_price
	FROM price_history_dwh.calendar c
	LEFT JOIN price_history_dwh.f_prices f 
		ON f.time_id =c.time_id
	LEFT JOIN price_history_dwh.product p
		ON f.product_id  = p.product_id 
	LEFT JOIN price_history_dwh.shop s 
		ON f.shop_id = s.shop_id
	WHERE c.year_week = to_char (current_date, 'IYYY_IW') 
	GROUP BY p.product_id ,p.product_full_name , s.shop_name , c.year_week
)
INSERT INTO price_history_dwh.weekly_agg(product_id, product_name, shop_name, year_week, min_sale_price, max_sale_price, avg_sale_price)
SELECT product_id ,
	product_name,
	shop_name,
	year_week,
	min_price,
	max_price,
	avg_price
FROM weekly
ON CONFLICT (product_id ,product_name, shop_name, year_week) 
DO UPDATE SET product_id = excluded.product_id ,
		product_name=excluded.product_name, 
		shop_name=excluded.shop_name,
		year_week=excluded.year_week,
		min_sale_price = excluded.min_sale_price,
		max_sale_price = excluded.max_sale_price,
		avg_sale_price = excluded.avg_sale_price;