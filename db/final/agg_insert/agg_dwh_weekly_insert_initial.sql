INSERT INTO price_history_dwh.weekly_agg(
    product_id,
    product_name,
    shop_name,
    year_week,
    min_sale_price,
    max_sale_price,
    avg_sale_price
)
SELECT
    price_history_dwh.product.product_id,
    price_history_dwh.product.product_full_name,
    price_history_dwh.shop.shop_name,
    price_history_dwh.calendar.year_week,
    min(price_history_dwh.f_prices.sale_price),
    max(price_history_dwh.f_prices.sale_price),
    avg(price_history_dwh.f_prices.sale_price)
FROM price_history_dwh.calendar 
LEFT JOIN price_history_dwh.f_prices 
	ON price_history_dwh.f_prices.time_id =price_history_dwh.calendar.time_id
LEFT JOIN price_history_dwh.product 
	ON price_history_dwh.f_prices.product_id  = price_history_dwh.f_product.product_id 
LEFT JOIN price_history_dwh.shop  
	ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id
WHERE price_history_dwh.calendar.date_date < current_date 
GROUP BY
    price_history_dwh.product.product_id,
    price_history_dwh.product.product_full_name,
    price_history_dwh.shop.shop_name,
    price_history_dwh.calendar.year_week;
