TRUNCATE TABLE price_history_dwh.last_year_agg;

INSERT INTO price_history_dwh.last_year_agg
WITH year_agg AS (
    SELECT
        price_history_dwh.f_prices.product_id,
        price_history_dwh.product.product_full_name,
        price_history_dwh.product.brand,
        price_history_dwh.shop.shop_name,
        price_history_dwh.calendar.date_date,
        price_history_dwh.f_prices.sale_price,
        min(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS min_y_price,
        max(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS max_y_price,
        avg(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS avg_y_price,
        first_value(
            price_history_dwh.calendar.date_date
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY price_history_dwh.calendar.date_date
        ) AS first_date,
        last_value(
            price_history_dwh.calendar.date_date
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY
                price_history_dwh.calendar.date_date
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
        ) AS last_date,
        first_value(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY price_history_dwh.calendar.date_date
        ) AS first_price,
        last_value(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY
                price_history_dwh.calendar.date_date
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
        ) AS current_price
    FROM price_history_dwh.f_prices
    INNER JOIN price_history_dwh.calendar
        ON
            price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id
    INNER JOIN price_history_dwh.product
        ON
            price_history_dwh.f_prices.product_id = price_history_dwh.product.product_id
    INNER JOIN price_history_dwh.shop
        ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id
    WHERE
        price_history_dwh.calendar.date_date > current_date - INTERVAL '12 month'
),

year_agg_min_date AS (
    SELECT
        year_agg.product_id,
        year_agg.shop_name,
        min(year_agg.date_date) AS min_y_price_date
    FROM year_agg
    WHERE year_agg.sale_price = year_agg.min_y_price
    GROUP BY year_agg.product_id, year_agg.shop_name
),

year_agg_max_date AS (
    SELECT
        year_agg.product_id,
        year_agg.shop_name,
        min(year_agg.date_date) AS max_y_price_date
    FROM year_agg
    WHERE year_agg.sale_price = year_agg.max_y_price
    GROUP BY year_agg.product_id, year_agg.shop_name
)

SELECT
    year_agg.product_id,
    year_agg.product_full_name,
    year_agg.brand,
    year_agg.shop_name,
    year_agg.min_y_price,
    year_agg.max_y_price,
    year_agg.avg_y_price,
    year_agg.first_date,
    year_agg.last_date,
    year_agg.first_price,
    year_agg.current_price,
    year_agg_min_date.min_y_price_date,
    year_agg_max_date.max_y_price_date,
    round(
        (year_agg.current_price / year_agg.first_price - 1) * 100, 2
    ) AS perc_price_diff
FROM year_agg
INNER JOIN year_agg_min_date
    ON
        year_agg.product_id = year_agg_min_date.product_id AND year_agg.shop_name = year_agg_min_date.shop_name
INNER JOIN year_agg_max_date
    ON
        year_agg.product_id = year_agg_max_date.product_id AND year_agg.shop_name = year_agg_max_date.shop_name
GROUP BY
    year_agg.product_id,
    year_agg.product_full_name,
    year_agg.brand,
    year_agg.shop_name,
    year_agg.min_y_price,
    year_agg.max_y_price,
    year_agg.avg_y_price,
    year_agg.first_date,
    year_agg.last_date,
    year_agg.first_price,
    year_agg.current_price,
    year_agg_min_date.min_y_price_date,
    year_agg_max_date.max_y_price_date;
