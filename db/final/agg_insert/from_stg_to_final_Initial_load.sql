set session my.etl = 'etl';

CREATE TEMPORARY TABLE temp_product(name_p varchar(200),name_p_1 varchar(200),name_p_2 varchar(200), Cellular varchar(10));
INSERT INTO temp_product(name_p) 
SELECT 
	DISTINCT 
	replace(replace(TRIM(name) , 'Zegarek typu smartwatch ', '') , 'Zegarek sportowy ', '')
FROM price_history_stg.products_stage;

UPDATE temp_product 
SET name_p_2 = right(name_p,length(name_p) - strpos(name_p , ' ')),
Cellular = case
		when POSITION('mm ' in name_p) > 0 then LTRIM(substring(name_p from POSITION('mm' in name_p) - 3 for 6))
		else null
	end;


INSERT INTO price_history_dwh.product (product_full_name,brand,product_second_part ,Cellular ,etl_name, process_name) 
SELECT 
	DISTINCT 
	upper(name_p),
	name_p_1,
	name_p_2,
	Cellular, 
	current_setting('my.etl')::VARCHAR, 
	user
FROM temp_product 
where not EXISTS (select from price_history_dwh.product where upper(temp_product.name_p) = product.product_full_name);


insert INTO price_history_dwh.shop (shop_name, shop_link, available, etl_name,process_name)
select
	DISTINCT 
	shop, 
		case 
				when shop = 'euro' then 'https://www.euro.com.pl/'
				when shop = 'morele' then 'https://www.morele.net/'
				when shop = 'mediaexpert' then 'https://www.mediaexpert.pl/'
				when shop = 'vobis' then 'https://vobis.pl/'
		end,
	true,
	current_setting('my.etl')::VARCHAR, 
	user
FROM price_history_stg.products_stage
where not EXISTS (select from price_history_dwh.shop where products_stage.shop = shop.shop_name);


INSERT INTO price_history_dwh.f_prices (product_id, shop_id, time_id, product_flag_id, sale_price, origin_price, rating,reviews_count,etl_name,process_name)
select
	product.product_id,
	shop.shop_id,
	CASE WHEN EXTRACT (HOUR FROM date) < 12 THEN CAST (concat(to_char (date, 'YYYYMMDD'), '00') AS int)
     	ELSE CAST (concat(to_char (date, 'YYYYMMDD'), '12') AS int)
    END,
	case 
	when products_stage.available = '0' then 2
	when products_stage.available = '1' then 1
	end,
	case 
		when sale_price = 'nan' then null
		else replace(sale_price,',','.')::numeric 
	end,
	case 
	  when origin_price = 'nan' AND sale_price <> 'nan' then sale_price::numeric 
	  WHEN origin_price = 'nan' AND sale_price = 'nan' then null
	  else replace(replace(replace(replace(origin_price, ')', ''), ' ', ''), ',', '.'), '(', '')::numeric 
	end,
	case when rating in ('nan', 'brak') then null
     when rating = '0' AND reviews_count = '0' then null
     else rating::numeric 
	end,
	case
		when reviews_count !~ '^[0-9]' then 0
		else reviews_count::integer
	end,
	current_setting('my.etl')::VARCHAR, 
	user
FROM price_history_stg.products_stage
inner join price_history_dwh.product  
	on product.product_full_name = upper(replace(replace(TRIM(name) , 'Zegarek typu smartwatch ', '') , 'Zegarek sportowy ', ''))
inner join price_history_dwh.shop
	on shop.shop_name = products_stage.shop
where not exists (select from price_history_dwh.f_prices fp2 where fp2.time_id = CASE WHEN EXTRACT (HOUR FROM date) < 12 THEN CAST (concat(to_char (date, 'YYYYMMDD'), '00') AS int)
     	ELSE CAST (concat(to_char (date, 'YYYYMMDD'), '12') AS int)
    end 
	and fp2.product_id = product.product_id
    and fp2.shop_id = shop.shop_id);

DROP TABLE temp_product;
