TRUNCATE TABLE price_history_dwh.last_week_agg;

INSERT INTO price_history_dwh.last_week_agg
WITH week_agg AS (
    SELECT
        price_history_dwh.f_prices.product_id,
        price_history_dwh.product.product_full_name,
        price_history_dwh.product.brand,
        price_history_dwh.shop.shop_name,
        price_history_dwh.calendar.date_date,
        price_history_dwh.f_prices.sale_price,
        min(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS min_w_price,
        max(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS max_w_price,
        avg(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
        ) AS avg_w_price,
        first_value(
            price_history_dwh.calendar.date_date
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY price_history_dwh.calendar.date_date
        ) AS first_date,
        last_value(
            price_history_dwh.calendar.date_date
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY
                price_history_dwh.calendar.date_date
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
        ) AS last_date,
        first_value(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY price_history_dwh.calendar.date_date
        ) AS first_price,
        last_value(
            price_history_dwh.f_prices.sale_price
        ) OVER(
            PARTITION BY
                price_history_dwh.f_prices.product_id,
                price_history_dwh.shop.shop_name
            ORDER BY
                price_history_dwh.calendar.date_date
            RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
        ) AS current_price
    FROM price_history_dwh.f_prices
    INNER JOIN price_history_dwh.calendar
        ON
            price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id
    INNER JOIN price_history_dwh.product
        ON
            price_history_dwh.f_prices.product_id = price_history_dwh.product.product_id
    INNER JOIN price_history_dwh.shop
        ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id
    WHERE price_history_dwh.calendar.date_date > current_date - INTERVAL '8 day'
),

week_agg_min_date AS (
    SELECT
        week_agg.product_id,
        week_agg.shop_name,
        min(week_agg.date_date) AS min_w_price_date
    FROM week_agg
    WHERE week_agg.sale_price = week_agg.min_w_price
    GROUP BY week_agg.product_id, week_agg.shop_name
),

week_agg_max_date AS (
    SELECT
        week_agg.product_id,
        week_agg.shop_name,
        min(week_agg.date_date) AS max_w_price_date
    FROM week_agg
    WHERE week_agg.sale_price = week_agg.max_w_price
    GROUP BY week_agg.product_id, week_agg.shop_name
)

SELECT
    week_agg.product_id,
    week_agg.product_full_name,
    week_agg.brand,
    week_agg.shop_name,
    week_agg.min_w_price,
    week_agg.max_w_price,
    week_agg.avg_w_price,
    week_agg.first_date,
    week_agg.last_date,
    week_agg.first_price,
    week_agg.current_price,
    week_agg_min_date.min_w_price_date,
    week_agg_max_date.max_w_price_date,
    round(
        (week_agg.current_price / week_agg.first_price - 1) * 100, 2
    ) AS perc_price_diff
FROM week_agg
INNER JOIN week_agg_min_date
    ON
        week_agg.product_id = week_agg_min_date.product_id AND week_agg.shop_name = week_agg_min_date.shop_name
INNER JOIN week_agg_max_date
    ON
        week_agg.product_id = week_agg_max_date.product_id AND week_agg.shop_name = week_agg_max_date.shop_name
GROUP BY
    week_agg.product_id,
    week_agg.product_full_name,
    week_agg.brand,
    week_agg.shop_name,
    week_agg.min_w_price,
    week_agg.max_w_price,
    week_agg.avg_w_price,
    week_agg.first_date,
    week_agg.last_date,
    week_agg.first_price,
    week_agg.current_price,
    week_agg_min_date.min_w_price_date,
    week_agg_max_date.max_w_price_date;
