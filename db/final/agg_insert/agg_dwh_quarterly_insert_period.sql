WITH quarterly as(
	SELECT p.product_full_name product_name,
		s.shop_name shop_name,
		c.year_quarter year_quarter,
		min(f.sale_price) min_price,
		max(f.sale_price) max_price,
		avg(f.sale_price) avg_price,
		stddev_samp(f.sale_price) std_price,
		CAST(sum(CASE WHEN (f.origin_price - f.sale_price) = 0 OR f.sale_price IS NULL THEN 0 ELSE 1 END)/2 AS int) discount_days ,
		cast(sum(CASE WHEN pf.available THEN 1 ELSE 0 END )/2 AS int) available_days
	FROM price_history_dwh.f_prices f
	INNER JOIN price_history_dwh.calendar c 
		ON f.time_id =c.time_id
	INNER JOIN price_history_dwh.product p
		ON f.product_id  = p.product_id 
	INNER JOIN price_history_dwh.shop s 
		ON f.shop_id = s.shop_id
	INNER JOIN price_history_dwh.product_flag pf 
		ON f.product_flag_id = pf.product_flag_id 
	WHERE c.year_quarter = to_char (current_date, 'YYYY_QQ') 
	GROUP BY p.product_full_name , s.shop_name , c.year_quarter
)
INSERT INTO price_history_dwh.quarterly_agg(product_name, shop_name, year_quarter, min_sale_price, max_sale_price, avg_sale_price, std_sale_price, discount_days, available_count_days)
SELECT product_name,
	shop_name,
	year_quarter,
	min_price,
	max_price,
	avg_price,
	std_price,
	discount_days,
	available_days
FROM quarterly
ON CONFLICT (product_name, shop_name, year_quarter) 
DO UPDATE SET product_name=excluded.product_name, 
		shop_name=excluded.shop_name,
		year_quarter=excluded.year_quarter,
		min_sale_price = excluded.min_sale_price,
		max_sale_price = excluded.max_sale_price,
		avg_sale_price = excluded.avg_sale_price,
		std_sale_price = excluded.std_sale_price,
		discount_days = excluded.discount_days,
		available_count_days = excluded.available_count_days;