INSERT INTO price_history_dwh.quarterly_agg(
    product_name,
    shop_name,
    year_quarter,
    min_sale_price,
    max_sale_price,
    avg_sale_price,
    std_sale_price,
    discount_days,
    available_count_days
)
SELECT
    price_history_dwh.product.product_full_name,
    price_history_dwh.shop.shop_name,
    price_history_dwh.calendar.year_quarter,
    min(price_history_dwh.f_prices.sale_price),
    max(price_history_dwh.f_prices.sale_price),
    avg(price_history_dwh.f_prices.sale_price),
    stddev_samp(price_history_dwh.f_prices.sale_price),
    cast(
        sum(
            CASE
                WHEN
                    (
                        price_history_dwh.f_prices.origin_price - price_history_dwh.f_prices.sale_price
                    ) = 0 OR price_history_dwh.f_prices.sale_price IS NULL THEN 0
                ELSE 1
            END
        ) / 2 AS INT
    ),
    cast(
        sum(
            CASE WHEN price_history_dwh.product_flag.available THEN 1 ELSE 0 END
        ) / 2 AS INT
    )
FROM price_history_dwh.f_prices
INNER JOIN price_history_dwh.calendar
    ON price_history_dwh.f_prices.time_id = price_history_dwh.calendar.time_id
INNER JOIN price_history_dwh.product
    ON
        price_history_dwh.f_prices.product_id = price_history_dwh.product.product_id
INNER JOIN price_history_dwh.shop
    ON price_history_dwh.f_prices.shop_id = price_history_dwh.shop.shop_id
INNER JOIN price_history_dwh.product_flag
    ON
        price_history_dwh.f_prices.product_flag_id = price_history_dwh.product_flag.product_flag_id
GROUP BY
    price_history_dwh.product.product_full_name,
    price_history_dwh.shop.shop_name,
    price_history_dwh.calendar.year_quarter;
