DROP TABLE IF EXISTS price_history_dwh.product;
CREATE TABLE price_history_dwh.product (
    product_id SERIAL PRIMARY KEY NOT NULL,
    product_full_name VARCHAR(200) NOT NULL,
    brand VARCHAR(50),
    product_second_part VARCHAR(200),
    cellular VARCHAR(10),
    etl_name VARCHAR(30),
    process_name VARCHAR(30),
    color VARCHAR(50),
    insert_date_tms TIMESTAMP DEFAULT current_timestamp,
    update_date_tms TIMESTAMP DEFAULT current_timestamp
);
