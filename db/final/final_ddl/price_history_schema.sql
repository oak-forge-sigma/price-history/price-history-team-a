DROP SCHEMA IF EXISTS price_history_dwh CASCADE;
CREATE SCHEMA price_history_dwh;

-- create user
DROP ROLE IF EXISTS price_history_f_group;
DROP ROLE IF EXISTS price_history_f_user;

CREATE ROLE price_history_f_user WITH login PASSWORD '';
CREATE ROLE price_history_f_group;

GRANT CONNECT ON DATABASE postgres TO price_history_f_group;
GRANT ALL ON SCHEMA price_history_dwh TO price_history_f_group;

GRANT price_history_f_group TO price_history_f_user;

ALTER SCHEMA price_history_dwh OWNER TO price_history_f_group;
