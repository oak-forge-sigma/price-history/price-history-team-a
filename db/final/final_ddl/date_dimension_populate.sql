INSERT INTO price_history_dwh.calendar (time_id,
                                        date_date,
                                        year_int,
                                        month_int,
                                        day_int,
                                        part_of_day,
                                        day_of_week,
                                        dayname_of_week,
                                        day_of_year,
                                        month_name,
                                        quarter_of_year,
                                        week_of_year,
                                        year_quarter,
                                        year_month,
                                        year_week,
                                        weekday_weekend,
                                        last_day_of_month)
(SELECT
        CAST(TO_CHAR(t, 'YYYYMMDDHH24') AS INT),
        CAST(t AS DATE),
        EXTRACT(YEAR FROM t),
        EXTRACT(MONTH FROM t),
        EXTRACT(DAY FROM t),
        CASE WHEN EXTRACT(HOUR FROM t) = 0 THEN 'AM'
            ELSE 'PM'
        END,
        EXTRACT(isodow FROM t),
        trim(TO_CHAR(t, 'Day')),
        EXTRACT(doy FROM t),
        trim(TO_CHAR(t, 'Month')),
        EXTRACT(QUARTER FROM t),
        EXTRACT(WEEK FROM t),
        TO_CHAR(t, 'YYYY_"Q"Q'),
        TO_CHAR(t, 'YYYY_MM'),
        TO_CHAR(t, 'IYYY-IW'),
        CASE WHEN EXTRACT(isodow FROM t) IN (6, 7) THEN 'weekend'
            ELSE 'weekday'
        END,
        CAST((DATE_TRUNC('month', t) + INTERVAL '1 month - 1 day') AS DATE)
    FROM GENERATE_SERIES('2021-12-10'::TIMESTAMP, '2023-12-31', '12hours'::INTERVAL) AS t);
