DROP TABLE IF EXISTS price_history_dwh.calendar;
CREATE TABLE price_history_dwh.calendar(
    time_id INT PRIMARY KEY NOT NULL,
    date_date DATE NOT NULL,
    year_int INT NOT NULL,
    month_int SMALLINT NOT NULL,
    day_int SMALLINT NOT NULL,
    part_of_day CHAR(2) NOT NULL,
    day_of_week SMALLINT NOT NULL,
    dayname_of_week VARCHAR(10) NOT NULL,
    day_of_year SMALLINT NOT NULL,
    month_name VARCHAR(10) NOT NULL,
    quarter_of_year SMALLINT NOT NULL,
    week_of_year SMALLINT NOT NULL,
    year_quarter CHAR(7) NOT NULL,
    year_month CHAR(7) NOT NULL,
    year_week CHAR(7)NOT NULL,
    weekday_weekend CHAR(7) NOT NULL,
    last_day_of_month DATE NOT NULL
);
