DROP TABLE IF EXISTS price_history_dwh.product_flag;
CREATE TABLE price_history_dwh.product_flag (
    product_flag_id SERIAL PRIMARY KEY NOT NULL,
    available BOOLEAN NOT NULL,
    is_delated BOOLEAN NOT NULL
);
INSERT INTO price_history_dwh.product_flag (available, is_delated)
VALUES
(TRUE, FALSE),
(FALSE, FALSE),
(TRUE, TRUE),
(FALSE, TRUE);
