DROP TABLE IF EXISTS price_history_dwh.f_prices;
CREATE TABLE price_history_dwh.f_prices(
	id_prices SERIAL PRIMARY KEY,
	product_id INT NOT NULL,
	shop_id INT NOT NULL,
	time_id INT,
	product_flag_id INT,
	sale_price NUMERIC(8,2),
	origin_price NUMERIC(8,2),
	rating NUMERIC(2, 1),
	reviews_count SMALLINT,
	etl_name VARCHAR(30),
	process_name VARCHAR(30),
	insert_date_tms TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    update_date_tms TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
ALTER TABLE price_history_dwh.f_prices OWNER TO price_history_dwh_group;
