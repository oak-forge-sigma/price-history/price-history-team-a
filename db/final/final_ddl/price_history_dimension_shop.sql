DROP TABLE IF EXISTS price_history_dwh.shop;
CREATE TABLE price_history_dwh.shop(
    shop_id SERIAL PRIMARY KEY NOT NULL,
    shop_name VARCHAR(30) NOT NULL,
    shop_link VARCHAR(50),
    available BOOLEAN NOT NULL,
    etl_name VARCHAR(30),
    process_name VARCHAR(30),
    insert_date_tms TIMESTAMP DEFAULT current_timestamp,
    update_date_tms TIMESTAMP DEFAULT current_timestamp
);
