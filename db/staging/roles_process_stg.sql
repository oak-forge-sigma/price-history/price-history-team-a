DROP ROLE IF EXISTS price_history_processes;
CREATE ROLE price_history_processes;

GRANT CONNECT ON DATABASE price_history_team_a TO price_history_processes;

GRANT ALL PRIVILEGES ON SCHEMA price_history_stg TO price_history_processes;

DROP ROLE IF EXISTS user_etl;
CREATE ROLE user_etl WITH login PASSWORD ;

GRANT price_history_processes TO user_etl;

ALTER TABLE price_history_stg.products_stage OWNER TO price_history_processes;

