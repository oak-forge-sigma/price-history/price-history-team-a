DROP TABLE IF EXISTS price_history_stg.products_stage;
CREATE TABLE price_history_stg.products_stage(

    name VARCHAR(200) NOT NULL,
    sale_price VARCHAR(20),
    origin_price VARCHAR(20),
    rating VARCHAR(20),
    reviews_count VARCHAR(20),
    available VARCHAR(20),
    date TIMESTAMP NOT NULL,
    shop VARCHAR(30),
    stg_insert_time TIMESTAMP DEFAULT current_timestamp,
	stg_insert_user VARCHAR(30) DEFAULT current_user,
    source_file VARCHAR(50) NOT NULL
);

